const Router = require('express').Router();

const {
    ServerError
} = require('../errors');

const {
    getAll,
    getById,
    add,
    update,
    remove
} = require('./services.js');

const {
    authorizeUser
} = require('../authorization');

const {
    ADMIN_ROLE,
    READER_ROLE
} = process.env;

Router.get('/:id', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    const author = await getById(id);

    res.json(author);

});

Router.get('/', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {

    const authors = await getAll();

    res.json(authors);

});

Router.post('/', authorizeUser(ADMIN_ROLE), async (req, res) => {
    const {
        nume
    } = req.body;

    if (typeof nume !== 'string') {
        throw new ServerError('Nume invalid!', 400);
    }

    const id = await add(nume);

    res.json({id, nume});
});

Router.put('/:id', authorizeUser(ADMIN_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    const {
        nume
    } = req.body;

    await update(id, nume);

    res.json({id, nume});

});

Router.delete('/:id', authorizeUser(ADMIN_ROLE), async (req, res) => {
    const {
        id
    } = req.params;

    await remove(id);

    res.status(200).end();
});

module.exports = Router;