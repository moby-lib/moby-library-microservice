# Moby Library Microservice

The Library microservice for the Moby Lib project.

## REST API

|Method|Path|Description|Req Body|Res Body|Minimum Role|
|---|---|---|---|---|---|
| GET | /api/library/authors/ | Returns a list of all authors in library. |  | [{"id": <author_id>, "nume": <author_name>}, ...] | READER |
| GET | /api/library/authors/:id | Returns the author with the given id. |  | {"id": <author_id>, "nume": <author_name>} | READER |
| POST | /api/library/authors/ | Add an author to the library. | {"nume": <author_name>} | {"id": <author_id>, "nume": <author_name>} | ADMIN |
| PUT | /api/library/authors/:id | Updates the author with given id. | {"nume": <author_name>} | {"id": <author_id>, "nume": <author_name>} | ADMIN |
| DELETE | /api/library/authors/:id | Deletes the author with given id. |  |  | ADMIN |
| GET | /api/library/books/ | Returns a list of all books in library. |  | [{"id": <book_id>, "nume": <book_name>, "genre": <book_genre>, "author": {<author_json_entity>}}, ...] | READER |
| GET | /api/library/books/:id | Returns the book with the given id. |  | {"id": <book_id>, "nume": <book_name>, "genre": <book_genre>, "author": {<author_json_entity>}} | READER |
| GET | /api/library/books/user | Returns a list of all books added by this user. |  | [{"id": <book_id>, "nume": <book_name>, "genre": <book_genre>, "author": {<author_json_entity>}}, ...] | READER |
| POST | /api/library/books/ | Add a book to the library. | {"nume": <book_name>, "genre": <book_genre>, "id_autor": <author_id>} | {"id": <book_id>} | READER |
| PUT | /api/library/books/:id | Updates the book with given id. | {"nume": <book_name>, "genre": <book_genre>, "id_autor": <author_id>} | {"id": <book_id>, "nume": <book_name>, "genre": <book_genre>, "id_autor": <author_id>} | READER |
| DELETE | /api/library/books/:id | Deletes the book with given id. |  |  | READER |
| DELETE | /api/library/books/user | Deletes the books for this user. |  |  | READER |
| DELETE | /api/library/books/user/:id | **INTERNAL**. Deletes the books for the user with given id. |  |  | None |


## Deployment

### Monorepo configurations - RECOMMENDED

[monorepo](https://gitlab.com/moby-lib/moby-monorepo)

### Using Docker - NOT RECOMMENDED
```
sudo docker network create library-net
sudo docker run --name moby-library-microservice --network="library-net" --env-file .env -p 3000:3000 registry.gitlab.com/moby-lib/moby-library-microservice:1.0
sudo docker network connect microservices-net moby-library-microservice

cd database-deploy; sudo docker-compose up
sudo docker network connect library-net database-deploy_library-db_1
```